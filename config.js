module.exports = {
    hass: {
        host: "192.168.1.5",
        port: "8123",
        token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjNjU0YzQzYjgyMTA0ZTk1ODMwZDc2MzViYTkyNDdmZCIsImlhdCI6MTYwMTcyODg0NCwiZXhwIjoxOTE3MDg4ODQ0fQ.Gn7yvAljV8bYojx8Jxfs6WnebWrGBU2OTSjEjRaaHYs"
    },
    bugsnag: {
        key: "a9fd025bba1b204b50ec95a52ce62ed4"
    },
    devices: [
        {
            name: 'tuya_switch_3',
            id: '37685021bcddc28b9c68',
            key: 'eab9a3d7df762087',
            hostname: '',
            enabled: true
        },
        {
            name: 'tuya_switch_8',
            id: '40605414bcddc28b99d4',
            key: '6c9beafdf3472718',
            hostname: '',
            enabled: false
        },
        {
            name: 'tuya_switch_7',
            id: '80020737b4e62d42508b',
            key: 'efba5f3bda8f10bb',
            hostname: '',
            enabled: true
        },
        {
            name: 'tuya_switch_5',
            id: '40605414bcddc28b9bb5',
            key: '5523306395d9a2b5',
            hostname: '',
            enabled: true
        },
        {
            name: 'tuya_switch_6',
            id: '40605414bcddc28b9a3c',
            key: 'eb0110370e42e2aa',
            hostname: '',
            enabled: false
        },
        {
            name: 'tuya_switch_4',
            id: '80020737bcddc28b9b27',
            key: '9d745a7411987eee',
            hostname: '',
            enabled: false
        },
        {
            name: 'tuya_switch_2',
            id: '37685021bcddc28b9bfa',
            key: '7d0bc9ffec0c61a9',
            hostname: '',
            enabled: true
        },
        {
            name: 'tuya_switch_1',
            id: '80020737bcddc28b9b33',
            key: 'c12c565739c07163',
            hostname: '',
            enabled: true
        }]
};
