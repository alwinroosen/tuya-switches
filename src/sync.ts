import Bugsnag from '@bugsnag/js'
import axios from 'axios'

const debug = require('debug')('TuyaSwitches')
const TuyaAPI = require('tuyapi')
const config: Config = require('./config')

Bugsnag.start({ apiKey: config.bugsnag.key })

export interface Device {
    name: string
    id: string
    key: string
    enabled: boolean
}

export interface Config {
    hass: {
        host: string
        port: number
        token: string
    }
    bugsnag: {
        key: string
    }
    devices: Device[]
}

interface TuyaDeviceConfig {
    id: string
    key: string
}

interface TuyaDeviceData {
    devId: string
    dps: {
        '1': boolean
        '2': number
        '4': number
        '5': number
        '6': number
    }
}

class DeviceInstance {
    private readonly device: Device
    private readonly instance: typeof TuyaAPI
    private connected = false

    constructor(device: Device) {
        this.device = device
        this.instance = new TuyaAPI({
            id: device.id,
            key: device.key,
        } as TuyaDeviceConfig)

        this.instance.on('connected', () => {
            console.info(`Device ${this.device.name} is connected`)
            this.connected = true
        })

        this.instance.on('disconnected', () => {
            this.connected = false
            console.warn(`Device ${this.device.name} is disconnected, reconnecting in 30 seconds`)
            setTimeout(() => {
                this.connect()
            }, 30000)
        })

        this.instance.on('error', (err: Error) => {
            console.error(`Received an error from device ${this.device.name}`)
            debug(err)
        })

        this.instance.on('data', (data: TuyaDeviceData) => {
            this.onReceiveData(data)
        })
    }

    public monitor() {
        console.log('Start monitoring device', this.device.name)
        if (!this.connected) {
            this.connect()
        }
    }

    public disconnect() {
        this.instance.disconnect()
    }

    private connect() {
        if (this.connected) {
            return
        }
        this.instance
            .find({
                timeout: 15,
            })
            .then(() => {
                this.instance.connect()
            })
            .catch((err: Error) => {
                console.error(`Unable to find device ${this.device.name}, trying again in 30 seconds`)
                debug(err)
                setTimeout(() => {
                    this.connect()
                }, 30000)
            })
    }

    private onReceiveData(data: TuyaDeviceData) {
        if (!data.dps) {
            throw new Error('No DPS values found: ' + JSON.stringify(data))
        }

        debug(`Received data for device ${this.device.name} : ${JSON.stringify(data)}`)

        if (data.dps.hasOwnProperty('1')) {
            sendDataEntry(this.device.name + '_state', data.dps['1'], 'On', '').then(() => {
                debug(`Update property state for ${this.device.name} to ${data.dps['1']}`)
            })
        }

        if (data.dps.hasOwnProperty('2')) {
            sendDataEntry(this.device.name + '_consumption', data.dps['2'], 'Consumption', 'kWh').then(() => {
                debug(`Update property consumption for ${this.device.name} to ${data.dps['2']}`)
            })
        }

        if (data.dps.hasOwnProperty('4')) {
            sendDataEntry(this.device.name + '_current', data.dps['4'], 'Current', 'A').then(() => {
                debug(`Update property current for ${this.device.name} to ${data.dps['4']}`)
            })
        }

        if (data.dps.hasOwnProperty('5')) {
            const value = data.dps['5'] / 10
            sendDataEntry(this.device.name + '_power', value, 'Power', 'W').then(() => {
                debug(`Update property power for ${this.device.name} to ${value}`)
            })
        }

        if (data.dps.hasOwnProperty('6')) {
            const value = data.dps['6'] / 10
            sendDataEntry(this.device.name + '_voltage', value, 'Voltage', 'V').then(() => {
                debug(`Update property voltage for ${this.device.name} to ${value}`)
            })
        }
    }
}

const sendDataEntry = (entityId: string, value: boolean | number, name: string, uom: string): Promise<boolean> => {
    const axiosConfig = {
        headers: {
            Authorization: 'Bearer ' + config.hass.token,
        },
    }
    const request = {
        state: value,
        attributes: {
            unit_of_measurement: uom,
            friendly_name: name,
        },
    }
    debug(
        '[' +
            entityId +
            '] http://' +
            config.hass.host +
            ':' +
            config.hass.port +
            '/api/states/sensor.' +
            entityId +
            ' with ' +
            JSON.stringify(request),
    )
    return axios
        .post(
            'http://' + config.hass.host + ':' + config.hass.port + '/api/states/sensor.' + entityId,
            request,
            axiosConfig,
        )
        .then((response) => {
            debug('[' + entityId + '] Response: ' + response.status)
            return true
        })
        .catch((err) => {
            console.error(err)
            return false
        })
}

const run = () => {
    const devices = config.devices
        .filter((_) => _.enabled)
        .map((device) => {
            return new DeviceInstance(device)
        })
    process.on('SIGINT', () => {
        devices.forEach((_) => _.disconnect())
        process.exit(0)
    })
    devices.forEach((_) => _.monitor())
}

run()
